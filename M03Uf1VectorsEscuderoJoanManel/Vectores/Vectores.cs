﻿using System;

namespace Vectors
{
    class Vectors
    {
        //Imprime un dia de la setmana
        public void DayOfWeek()
        {
            string[] days = new string[7] { "Dilluns", "Dimarts", "Dimecres", "Dijous", "Divendres", "Dissabte", "Diumenge" };
            Console.WriteLine("Introduce el número del 1 al 7 para escoger un dia de la semana.");
            int dia = Convert.ToInt32(Console.ReadLine());
            if (dia > 6 || dia < 0)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("ERROR 404:No es un dia valido");
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("Introduce el número del 1 al 7 para escoger un dia de la semana.");
                dia = Convert.ToInt32(Console.ReadLine());
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(days[dia - 1]);
        }

        //Guardar e imprimir el número de los jugadors
        public void PlayNumbers()
        {
            int[] alineacio = new int[5];

            for (int i = 0; i < 5; i++)
            {
                Console.WriteLine("Introduce un número de la alineación.");
                int num = Convert.ToInt32(Console.ReadLine());
                alineacio[i] = num;
            }
            Console.Write("[");
            for (int j = 0; j < 5; j++)
            {
                Console.Write(alineacio[j]);
                if (j < 4)
                {
                    Console.Write(", ");
                }
            }
            Console.WriteLine("]");
        }

        //Programa per a partit politic
        public void CandidatesList()
        {
            Console.WriteLine("Introduce el número de candidatos.");
            int num = Convert.ToInt32(Console.ReadLine());
            string[] candidato = new string[num];
            for (int i = 0; i < num; i++)
            {
                Console.WriteLine("Introduce un candidato.");
                string name = Console.ReadLine();
                candidato[i] = name;
            }
            int posicion;
            do
            {
                Console.WriteLine("Introduce la posicion del candidato.");
                posicion = Convert.ToInt32(Console.ReadLine());
                if (posicion != -1)
                {
                    Console.WriteLine(candidato[posicion - 1]);
                }
            } while (posicion != -1);
        }

        //Donada una paraula i una posició  indica quina lletra hi ha a la posició indicada
        public void LetterInWord()
        {
            Console.WriteLine("Pon una palabra");
            string palabra = Console.ReadLine();
            Console.WriteLine("Dime una posicion de la letraA");
            int posicion = 0;

            do
            {
                posicion = Convert.ToInt32(Console.ReadLine());
            } while (posicion >= palabra.Length || posicion < 0);

            Console.WriteLine(palabra[posicion - 1]);
        }

        // Inicialitza un vector de floats de mida 50, amb el valor 0.0f a tots els elements.
        public void AddValuesToList()
        {
            int x = 0;
            float[] numeros = new float[50];
            for (int i = 0; i < 50; i++)
            {
                numeros[i] = 0.0f;
            }
            Console.Write("[");
            for (int i = 0; i < 50; i++)
            {
                switch (i)
                {
                    case 0:
                        numeros[i] = 31.0f;
                        break;
                    case 1:
                        numeros[i] = 56.0f;
                        break;
                    case 19:
                        numeros[i] = 12.0f;
                        break;
                    case 49:
                        numeros[i] = 79.0f;
                        break;
                    default:
                        break;
                }
                Console.Write(numeros[i] + ".0");
                if (x < 49)
                {
                    Console.Write(" , ");
                    x++;
                }
            }
            Console.WriteLine("]");
        }

        //Donada un vector de 4 números de tipus int,intercanvia el primer per l'últim element.
        public void Swap()
        {
            int guardar;
            Console.WriteLine("Pon 4 numeros");
            int[] numeros = new int[4];
            for (int i = 0; i < 4; i++)
            {
                int num = Convert.ToInt32(Console.ReadLine());
                numeros[i] = num;
            }
            Console.Write("Tu listado de numeros es:[ ");
            for (int n = 0; n < 4; n++)
            {
                int x = 0;
                Console.Write(numeros[n]);
                if (x < 3)
                {
                    Console.Write(" , ");
                    x++;
                }
            }
            Console.WriteLine("]");
            Console.Write("Vamos a cambiar el primer y ultimo numero: [  ");
            guardar = numeros[0];
            numeros[0] = numeros[3];
            numeros[3] = guardar;
            for (int n = 0; n < 4; n++)
            {
                int x = 0;
                Console.Write(numeros[n]);
                if (x < 3)
                {
                    Console.Write(" , ");
                    x++;
                }
            }
            Console.WriteLine("]");

        }

        //Volem fer un simulador d'un candau :
        public void PushButtonPadlockSimulator()
        {
            Boolean[] candado = new bool[8] { false, false, false, false, false, false, false, false };
            int num;
            do
            {
                Console.WriteLine("Pon los numeros del candado");
                num = Convert.ToInt32(Console.ReadLine());
                if (num != -1)
                {
                    candado[num - 1] = !candado[num - 1];
                }
            } while (num != -1);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("[");
            for (int i = 0; i < 8; i++)
            {
                int x = 0;
                Console.Write(candado[i]);
                if (x < 7)
                {
                    Console.Write(" , ");
                    x++;
                }
            }
            Console.WriteLine("]");
            Console.ForegroundColor = ConsoleColor.Cyan;
        }
        //Un banc té tot de caixes de seguretat, enumerades del 0 al 10.
        public void BoxesOpenedCounter()
        {
            int[] cajas = new int[10] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            int num;
            do
            {
                Console.WriteLine("Abre una caja");
                num = Convert.ToInt32(Console.ReadLine());
                if (num != -1)
                {
                    cajas[num - 1]++;
                }
            } while (num != -1);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Has abierto cada caja:");
            Console.WriteLine("                 Cajas     ");
            Console.WriteLine(" 1   2   3   4   5   6   7   8   9   10");
            Console.WriteLine("");
            Console.ForegroundColor = ConsoleColor.Green;

            Console.Write("[");
            for (int i = 0; i < 10; i++)
            {

                Console.Write(cajas[i]);
                if (i < 9)
                {
                    Console.Write(" , ");

                }

            }
            Console.WriteLine("]");
            Console.ForegroundColor = ConsoleColor.Cyan;
        }
        //Minimo de 10 numeros
        public void MinOf10Values()
        {
            int[] enteros = new int[10];
            Console.WriteLine("Introduce 10 numeros");
            for (int i = 0; i < enteros.Length; i++)
            {
                enteros[i] = Convert.ToInt32(Console.ReadLine());
            }
            int num = enteros[0];
            for (int x = 1; x < enteros.Length; x++)
            {
                if (enteros[x] < num) num = enteros[x];
            }
            Console.Write("El numero mas pequeño es: ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(num);
            Console.ForegroundColor = ConsoleColor.Cyan;
        }
        //Donat el següent vector, imprimeix true si algun dels números és divisible entre 7 o false en cas contrari.
        public void IsThereAMultipleOf7()
        {
            bool respuesta = false;
            int[] numeros = new int[] { 4, 8, 9, 40, 54, 84, 40, 6, 84, 1, 1, 68, 84, 68, 4, 840, 684, 25, 40, 98, 54, 687, 31, 4894, 468, 46, 84687, 894, 40, 846, 1681, 618, 161, 846, 84687, 6, 848 };
            for (int i = 0; i < numeros.Length; i++)
            {
                if (numeros[i] % 7 == 0)
                {
                    respuesta = true;
                }
            }
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("[ ");
            for (int i = 0; i < numeros.Length; i++)
            {
                Console.Write(numeros[i]);
                if (i < numeros.Length - 1)
                {
                    Console.Write(" , ");
                }
            }
            Console.WriteLine(" ]");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("Hemos buscado si hay multiple de 7 y la respuesta es: ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(respuesta);
            Console.ForegroundColor = ConsoleColor.Cyan;

        }
        //Donat una llista d'enters ordenats de menor a major indica si un cert valor existeix en el bucle.
        public void SearchInOrdered()
        {
            bool respuesta = false;
            int[] numeros;
            Console.WriteLine("cuantos numeros quieres introducir en tu vector?:");
            numeros = new int[Convert.ToInt32(Console.ReadLine())];
            Console.ForegroundColor = ConsoleColor.Yellow;
            for (int i = 0; i < numeros.Length; i++)
            {
                Console.WriteLine("Pon un numero");
                numeros[i] = Convert.ToInt32(Console.ReadLine());
            }
            Console.ForegroundColor = ConsoleColor.Cyan;
            int numero;
            Console.Write("Introduce un numero: ");
            numero = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < numeros.Length; i++)
            {
                if (numero == numeros[i]) respuesta = true;
            }
            Console.Write("Tu numero que es " + numero + " esta = ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(respuesta);
            Console.ForegroundColor = ConsoleColor.Cyan;
        }
        //L'usuari entra 10 enters. Imprimeix-los en l'ordre invers al que els ha entrat.
        public void InverseOrder()
        {
            int[] numeros = new int[10];
            Console.WriteLine("Introduce 10 numeros: ");
            for (int i = 0; i < numeros.Length; i++)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("Introduce un numero:");
                Console.ForegroundColor = ConsoleColor.Green;
                numeros[i] = Convert.ToInt32(Console.ReadLine());
            }
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Vamos a darle la vuelta!: ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("[");
            for (int x = numeros.Length; x > 0; x--)
            {
                Console.Write(numeros[x - 1]);
                if (x > 1) Console.Write(" , ");

            }
            Console.WriteLine("]");
        }

        //Indica si una paraula és palíndrom.
        public void Palindrome()
        {
            Console.WriteLine("Introduce una palabra ");
            bool palindromo = false;
            Console.ForegroundColor = ConsoleColor.Yellow;
            string palabra = Console.ReadLine();
            int final = palabra.Length - 1;
            for (int i = 0; i < palabra.Length; i++)
            {
                if (palabra[i] == palabra[final]) palindromo = true;
                else
                {
                    palindromo = false;
                    break;
                }
                final--;
            }
            Console.ForegroundColor = ConsoleColor.Cyan;
            if (palindromo == true)
            {
                Console.Write("La Palabra (" + palabra + ")");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(" es un palindromo!!");
            }
            else
            {
                Console.Write("La Palabra (" + palabra + ")");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(" no es un palindromo!!");
            }
            Console.ForegroundColor = ConsoleColor.Cyan;
        }

        //Printa per pantalla ordenats si la llista de N valors introduïts per l'usuari estan ordenats.
        public void ListSortedValues()
        {
            bool respuesta = false;
            Console.WriteLine("Cuantos numeros quieres introducir?:");
            int rango = Convert.ToInt32(Console.ReadLine());
            int[] ordenacion = new int[rango];
            for (int i = 0; i < ordenacion.Length; i++)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("Pon un numero:");
                Console.ForegroundColor = ConsoleColor.Green;
                ordenacion[i] = Convert.ToInt32(Console.ReadLine());
            }
            Console.ForegroundColor = ConsoleColor.Cyan;
            int guardar = ordenacion[0];
            for (int i = 0; i < ordenacion.Length; i++)
            {
                if (guardar <= ordenacion[i])
                {
                    guardar = ordenacion[i];
                    respuesta = true;
                }
                else
                {
                    respuesta = false;
                    break;

                }
            }
            if (respuesta == true)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("");
                Console.WriteLine("                 |-----------------------------------------|");
                Console.WriteLine("                 |        La lista esta ordenada!!         |");
                Console.WriteLine("                 |-----------------------------------------|");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("");
                Console.WriteLine("                 |-----------------------------------------|");
                Console.WriteLine("                 |        La lista no esta ordenada!!      |");
                Console.WriteLine("                 |-----------------------------------------|");

            }
            Console.ForegroundColor = ConsoleColor.Cyan;

        }

        //Printa per pantalla cap i cua si la llista de N valors introduïts per l'usuari són cap i cua (llegits en ordre invers és la mateixa llista).
        public void CapICuaValues()
        {
            bool respuesta = false;
            Console.Write("Cuantos numeros quieres introducir?=");
            Console.ForegroundColor = ConsoleColor.Green;
            int num = Convert.ToInt32(Console.ReadLine());
            Console.ForegroundColor = ConsoleColor.Cyan;
            int[] capicua = new int[num];
            for (int i = 0; i < capicua.Length; i++)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("Pon un numero:");
                Console.ForegroundColor = ConsoleColor.Green;
                capicua[i] = Convert.ToInt32(Console.ReadLine());
            }
            Console.ForegroundColor = ConsoleColor.Cyan;
            int final = capicua.Length - 1;
            for (int i = 0; i < capicua.Length; i++)
            {
                if (capicua[final] == capicua[i]) respuesta = true;
                else
                {
                    respuesta = false;
                    break;

                }
                final--;
            }
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("[");
            for (int i = 0; i < capicua.Length; i++)
            {
                Console.Write(capicua[i]);
                if (i < capicua.Length - 1)
                {
                    Console.Write(" , ");
                }
            }
            Console.WriteLine("]");
            if (respuesta == true)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("");
                Console.WriteLine("                 |-----------------------------------------|");
                Console.WriteLine("                 |                CAP I CUA                |");
                Console.WriteLine("                 |-----------------------------------------|");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("");
                Console.WriteLine("                 |-----------------------------------------|");
                Console.WriteLine("                 |             NO ES CAP I CUA             |");
                Console.WriteLine("                 |-----------------------------------------|");

            }
            Console.ForegroundColor = ConsoleColor.Cyan;
        }

        //L'usuari introduirà 2 vectors de valors, primer mida després introdueix elements.
        public void ListSameValues()
        {
            bool respuesta = false;
            Console.WriteLine("Cuantos numeros quieres introducir?=");
            int numeros = Convert.ToInt32(Console.ReadLine());
            int[] vector1 = new int[numeros];
            int[] vector2 = new int[numeros];
            for (int i = 0; i < vector1.Length; i++)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("Introduce un numero para vector 1:");
                Console.ForegroundColor = ConsoleColor.Green;
                vector1[i] = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("");
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.Write("Introduce un numero para vector 2:");
                Console.ForegroundColor = ConsoleColor.Green;
                vector2[i] = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("");
            }
            Console.ForegroundColor = ConsoleColor.Cyan;
            for (int i = 0; i < vector1.Length; i++)
            {
                if (vector1[i] == vector2[i]) respuesta = true;
                else
                {
                    respuesta = false;
                    break;
                }
            }
            if (respuesta == true)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("");
                Console.WriteLine("                 |-----------------------------------------|");
                Console.WriteLine("                 |                SON IGUALS               |");
                Console.WriteLine("                 |-----------------------------------------|");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("");
                Console.WriteLine("                 |-----------------------------------------|");
                Console.WriteLine("                 |                NO SON IGUALS            |");
                Console.WriteLine("                 |-----------------------------------------|");

            }
            Console.ForegroundColor = ConsoleColor.Cyan;
        }

        //Suma els valors
        public void ListSumValues()
        {
            Console.Write("Oye tu cuantos numeros quieres introducir  ?=");
            Console.ForegroundColor = ConsoleColor.Green;
            int num = Convert.ToInt32(Console.ReadLine());
            int[] numeros = new int[num];
            for (int i = 0; i < numeros.Length; i++)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("Pon un numero:");
                Console.ForegroundColor = ConsoleColor.Green;
                numeros[i] = Convert.ToInt32(Console.ReadLine());
            }
            int suma=0;
            for (int i = 0; i < numeros.Length; i++)
            {
                suma += numeros[i];
            }
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("La suma de los numeros de los vectores da un total de : ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(suma);
            Console.ForegroundColor = ConsoleColor.Cyan;

        }

        //Calcula el iva de 10 numeros
        public void IvaPrices()
        {
            Console.WriteLine("Bienvenido a la tienda de monos albinos tenemos una gran variedad de monos a escoger \n hecha un vistazo");
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("Mono albino en caballo 45 euros");
            Console.WriteLine("Mono flip 120 euros");
            Console.WriteLine("Mono Peruano 90 euros");
            Console.WriteLine("Mono tomando cafe con un capybara 190 euros");
            Console.WriteLine("Mono Reguetonero 50 euros");
            Console.WriteLine("Mono fumeton 60 euros");
            Console.WriteLine("Mono badaloniense 1 euro - ya que no podemos regalarlo");
            Console.WriteLine("Mono con saxofon 67 euros");
            Console.WriteLine("Mono a secas 15 euros");
            Console.WriteLine("Capybara disfrazado de mono 260 euros");
            double[] precios_monos = new double[10] {45,120,90,190,50,60,1,67,15,260};
            int i = 1;
            foreach (double x in precios_monos)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write("El precio con iva del mono " + i + " con su iva es de " );
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(x + x * 0.21 + " euros");
                i++;
            }
            Console.ForegroundColor = ConsoleColor.Cyan;

        }

        //Calcular casos de covid
        public void CovidGrowRate()
        {
            Console.WriteLine("Cuantos casos quieres introducir = ");
            int num = Convert.ToInt32(Console.ReadLine());
            decimal[] numeros = new decimal[num];
            for (int i = 0; i < numeros.Length; i++)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Write("Introduce un numero ");
                Console.ForegroundColor = ConsoleColor.Green;
                numeros[i] = Convert.ToDecimal(Console.ReadLine());
            }
            decimal[] GrowRate = new decimal[num];
            for (int i = 1; i < numeros.Length; i++)
            {
                GrowRate[i] = numeros[i] / numeros[i-1];
            }
            Console.Write("[");
            for (int i = 1; i < GrowRate.Length; i++)
            {
                Console.Write(GrowRate[i]);
                if (i < GrowRate.Length - 1)
                {
                    Console.Write(" , ");
                }
            }
            Console.WriteLine("]");
            Console.ForegroundColor = ConsoleColor.Cyan;

        }

        //Cuanta distancia podrem fer en 10 segons
        public void BicicleDistance()
        {
            Console.Write("Cuantos metros haces en 1 segundo?:");
            Console.ForegroundColor = ConsoleColor.Cyan;
            double segundo =Convert.ToDouble(Console.ReadLine());
            double[] segundos = new double[10];
            segundos[0]=segundo;
            for (int i = 1; i < segundos.Length;i++)
            {
                segundos[i] += (2.5*i);
            }
            Console.Write("[");
            for (int i = 1; i < segundos.Length; i++)
            {
                Console.Write(segundos[i]);
                if (i < segundos.Length - 1)
                {
                    Console.Write(" / ");
                }
            }
            Console.WriteLine("]");
            Console.ForegroundColor = ConsoleColor.Cyan;
        }

        //Averigua el numero mes aporximat a la mitjana
        public void ValueNearAvg()
        {
            Console.WriteLine("Cuanta cantidad de numeros quieres introducir");
            double media = 0;
            int numero_media = 0;
            Console.ForegroundColor = ConsoleColor.Green;
            int maximo = Convert.ToInt32(Console.ReadLine());
            double[] avg = new double[maximo];
            double[] lista = new double[maximo];
            for (int i = 0; i < maximo; i++)
            {
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.Write("Introduce un valor: ");
                Console.ForegroundColor = ConsoleColor.Green;
                lista[i] = Convert.ToDouble(Console.ReadLine());
            }
            for (int i = 0; i < maximo; i++)
            {
                media += lista[i];
            }
            media /= Convert.ToDouble(maximo);
            for (int i = 0; i < maximo; i++)
            {
                avg[i] = Math.Abs(lista[i]-media);
            }
            for(int i = 1; i < maximo; i++)
            {
                if (avg[i] > avg[i - 1])
                {
                    numero_media = i;
                }
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("El valor mas aproximado a la media es : " + lista[numero_media]);
            Console.ForegroundColor = ConsoleColor.Cyan;
        }

        //Calcula isbn
        public void Isbn()
        {
            bool cierto = false;
            Console.WriteLine("Introduce 10 numeros para calcular el ISBN");
            int[] isbn = new int[10];
            for (int i = 0; i < isbn.Length; i++)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.Write("Introduce el numero " + i + ":");
                Console.ForegroundColor = ConsoleColor.Green;
                isbn[i]=Convert.ToInt32(Console.ReadLine());
            }
            Console.ForegroundColor = ConsoleColor.Cyan;
            int resultado=0;
            for (int i = 1; i <= isbn.Length; i++)
            {
                if (i < 10)
                {
                    resultado += isbn[i - 1] * i;
                }
            }
            Console.WriteLine(resultado);
            if (resultado % 11 == isbn[9])
            {
                cierto= true;
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.Write("El resultado del isbn es : ");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(cierto);
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.Write("El resultado del isbn es : ");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(cierto);
            }
        }
        
        //Calcula isbn 13

        public void Isbn13()
        {
            Console.WriteLine("Hola,Introduce 13 numeros para poder calcular el ISBN 13:");
            int[] isbn13 = new int[13];
            bool respuesta = false;
            for (int i = 0; i < isbn13.Length; i++)
            {
                Console.ForegroundColor = ConsoleColor.DarkMagenta;
                Console.Write("Introduce un numero:");
                Console.ForegroundColor = ConsoleColor.Green;
                isbn13[i] = Convert.ToInt32(Console.ReadLine());
            }
            Console.ForegroundColor = ConsoleColor.Cyan;
            int guardar = 0;
            for (int i = 1; i < isbn13.Length; i++)
            {
                if ((i - 1) % 2 == 0)
                {
                    guardar += isbn13[i - 1] * 1;
                }
                else
                {
                    guardar += isbn13[i - 1] * 3;
                }
            }
            if ((guardar + isbn13[12]) % 10 == 0)
            {
                respuesta = true;
                Console.Write("El teu isbn13 es :");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(respuesta);
            }
            else
            {
                Console.Write("El teu isbn13 es :");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(respuesta);
            }
            Console.ForegroundColor = ConsoleColor.Cyan;
        }
        public void Menu()
        {
            string option;
            do
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("|---Hola! Et presento el meu menu de programes:---|");
                Console.WriteLine("|   1.-DayOfWeek                                  |");
                Console.WriteLine("|   2.-PlayNumbers                                |");
                Console.WriteLine("|   3.-CandidatesList                             |");
                Console.WriteLine("|   4.-LetterInWord                               |");
                Console.WriteLine("|   5.-AddValuesToList                            |");
                Console.WriteLine("|   6.-Swap                                       |");
                Console.WriteLine("|   7.-PushButtonPadlockSimulator                 |");
                Console.WriteLine("|   8.-BoxesOpenedCounter                         |");
                Console.WriteLine("|   9.-MinOf10Values                              |");
                Console.WriteLine("|   10.-IsThereAMultipleOf7                       |");
                Console.WriteLine("|   11.-SearchInOrdered                           |");
                Console.WriteLine("|   12.-InverseOrder                              |");
                Console.WriteLine("|   13.-Palindrome                                |");
                Console.WriteLine("|   14.-ListSortedValues                          |");
                Console.WriteLine("|   15.-CapICuaValues                             |");
                Console.WriteLine("|   16.-ListSameValues                            |");
                Console.WriteLine("|   17.-ListSumValues                             |");
                Console.WriteLine("|   18.-IvaPrices                                 |");
                Console.WriteLine("|   19.-CovidGrowRate                             |");
                Console.WriteLine("|   20.-BicicleDistance                           |");
                Console.WriteLine("|   21.-ValueNearAvg                              |");
                Console.WriteLine("|   22.-Isbn                                      |");
                Console.WriteLine("|   23.-Isbn13                                    |");
                Console.WriteLine("|   0.-EXIT                                       |");
                Console.WriteLine("|---Escull una opció:-----------------------------|");
                Console.ForegroundColor = ConsoleColor.Gray;
                option = Console.ReadLine();
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Clear();

                switch (option)
                {
                    case "1":
                        DayOfWeek();
                        break;
                    case "2":
                        PlayNumbers();
                        break;
                    case "3":
                        CandidatesList();
                        break;
                    case "4":
                        LetterInWord();
                        break;
                    case "5":
                        AddValuesToList();
                        break;
                    case "6":
                        Swap();
                        break;
                    case "7":
                        PushButtonPadlockSimulator();
                        break;
                    case "8":
                        BoxesOpenedCounter();
                        break;
                    case "9":
                        MinOf10Values();
                        break;
                    case "10":
                        IsThereAMultipleOf7();
                        break;
                    case "11":
                        SearchInOrdered();
                        break;
                    case "12":
                        InverseOrder();
                        break;
                    case "13":
                        Palindrome();
                        break;
                    case "14":
                        ListSortedValues();
                        break;
                    case "15":
                        CapICuaValues();
                        break;
                    case "16":
                        ListSameValues();
                        break;
                    case "17":
                        ListSumValues();
                        break;
                    case "18":
                        IvaPrices();
                        break;
                    case "19":
                        CovidGrowRate();
                        break;
                    case "20":
                        BicicleDistance();
                        break;
                    case "21":
                        ValueNearAvg();
                        break;
                    case "22":
                        Isbn();
                        break;
                    case "23":
                        Isbn13();
                        break;
                    case "0":
                        Console.WriteLine("Bye!");
                        break;
                    default:
                        Console.WriteLine("Opció incorrecta");
                        break;
                }
                Console.ReadLine();
            } while (option != "0");

        }
        static void Main()
        {
            var menu = new Vectors();
            menu.Menu();
        }
    }
}

