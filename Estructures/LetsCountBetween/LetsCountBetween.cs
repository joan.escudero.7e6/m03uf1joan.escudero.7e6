﻿using System;
//Author Joan Manel
//Date 12/10/22
//Description Contar entre dos numeros
namespace LetsCountBetween
{
    class LetsCountBetween
    {
        static void Main(string[] args)
        {
            int num1;
            int num2;
            Console.WriteLine("Pon dos numeros");
            num1 = Convert.ToInt32(Console.ReadLine());
            num2 = Convert.ToInt32(Console.ReadLine());
            num1++;
            Console.WriteLine("Los numeros entre estos dos son:");
            for (; num1 < num2; num1++)
            {
                Console.WriteLine(num1);
            }
        }
    }
}
