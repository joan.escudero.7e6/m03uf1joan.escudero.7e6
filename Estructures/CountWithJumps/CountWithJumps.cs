﻿using System;
//Author Joan Manel
//Date 13/10/22
//Description Contar con saltos
namespace CountWithJumps
{
    class CountWithJumps
    {
        static void Main(string[] args)
        {
            int final;
            int salt;
            Console.WriteLine("Pon dos numeros");
            final = Convert.ToInt32(Console.ReadLine());
            salt = Convert.ToInt32(Console.ReadLine());
            for (int num1 = 1; num1 <= final; num1=num1+salt) 
            {
                Console.Write(num1+" ");
            }

        }
    }
}
