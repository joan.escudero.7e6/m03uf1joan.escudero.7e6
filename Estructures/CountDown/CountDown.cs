﻿using System;
//Author Joan Manel
//Date 12/10/22
//Description Cuenta atras
namespace CountDown
{
    class CountDown
    {
        static void Main(string[] args)
        {
            int num;
            Console.WriteLine("Pon un numero");
            num = Convert.ToInt32(Console.ReadLine());
            for (; num > 0; num--)
            {
                Console.Write(num+" ");
            }
        }
    }
}
