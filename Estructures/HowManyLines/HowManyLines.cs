﻿using System;
//Author Joan Manel
//Date 6/10/22
//Description Cuantas lineas hay?
namespace HowManyLines
{
    class Program
    {
        static void Main(string[] args)
        {
            int numlineas = 0;
            string Frase;
            Console.WriteLine("Escribe una frase");
            Frase = Console.ReadLine();
            while (Frase != "END")
            {
                Frase = Console.ReadLine();
                numlineas++;
            }Console.WriteLine("Has escrito " + numlineas + " lineas");
        }
    }
}
