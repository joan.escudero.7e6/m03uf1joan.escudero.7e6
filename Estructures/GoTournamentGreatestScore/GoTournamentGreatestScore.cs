﻿using System;
//Author Joan Manel
//Date 18/10/22
//Description Maximo Puntuacion del torneo
namespace GoTournamentGreatestScore
{
    class GoTournamentGreatestScore
    {
        static void Main(string[] args)
        {
            int numpuntos ;
            int numpuntos2=0;
            int MaximaPunt = 0;
            string Frasenombre;
            string Nombre = "";

            Console.WriteLine("Escribe un nombre");
            Frasenombre = Console.ReadLine();
            Nombre = Frasenombre;
            Console.WriteLine("Escribe su puntuacion");
            numpuntos = Convert.ToInt32(Console.ReadLine());
            MaximaPunt = numpuntos;

            while (Frasenombre != "END")
            {
                Console.WriteLine("Escribe un nombre");
                Frasenombre = Console.ReadLine();
                if (Frasenombre == "END") 
                    break;
                Console.WriteLine("Escribe su puntuacion");
                numpuntos2 = numpuntos;
                numpuntos = Convert.ToInt32(Console.ReadLine());
                if (numpuntos > numpuntos2)
                {
                    MaximaPunt = numpuntos;
                    Nombre = Frasenombre;
                }
            }Console.WriteLine("La persona con mas puntuacion es " + Nombre + " con una puntuacion de " + MaximaPunt);
        }
    }
}
