﻿using System;
//Author Joan Manel
//Date 9/10/22
//Description Numero de 1 al 5 V2
namespace NumberBetween1and5v2
{
    class NumberBetV2
    {
        static void Main(string[] args)
        {
            int num;
            Console.WriteLine("Escribe un numero del 1 al 5");
            num = Convert.ToInt32(Console.ReadLine());
            do
            {
                Console.WriteLine("te dije del  1 al 5");
                num = Convert.ToInt32(Console.ReadLine());
            } while (num > 5 || num < 1);
            Console.WriteLine("El numero es " + num);

        }
    }
}
