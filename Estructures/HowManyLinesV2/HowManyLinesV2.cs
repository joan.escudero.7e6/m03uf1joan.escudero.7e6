﻿using System;
//Author Joan Manel
//Date 9/10/22
//Description Cuantas lineas hay?V2
namespace HowManyLinesV2
{
    class HowManyLinesV2
    {
        static void Main(string[] args)
        {
            int numlineas = 0;
            string Frase;
            Console.WriteLine("Escribe una frase");
            Frase = Console.ReadLine();
            do
            {
                Frase = Console.ReadLine();
                numlineas++;
            } while (Frase != "END");
            Console.WriteLine("Has escrito " + numlineas + " lineas");
        }
    }
}
