﻿using System;
//Author Joan Manel
//Date 22/10/22
//Description Es primo?
namespace IsPrime
{
    class IsPrime
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduce un numero");
            int num = Convert.ToInt32(Console.ReadLine());
            int cont = 0;
            int primo = 0;
            int i = 1;
            while (i <= num)
            {
                cont = (num % i);
                if (cont == 0)
                {
                    primo++;
                }
                i++;
            }
            if (primo == 2)
            {
                Console.WriteLine("Es primo");
            }
            else
            {
                Console.WriteLine("No es primo");
            }
        }
    }
}
