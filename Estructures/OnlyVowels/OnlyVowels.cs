﻿using System;
//Author Joan Manel
//Date 13/10/22
//Description Solo vocales
namespace OnlyVowels
{
    class OnlyVowels
    {
        static void Main(string[] args)
        {
            int num;
            string vocales = "";
            Console.WriteLine("Posa un numero");
            num = Convert.ToInt32(Console.ReadLine());

            for (int num2 = 0; num2 <= num*2; num2++)
            {
                int lletra = Console.Read();
               if (lletra == 97 || lletra == 101 || lletra == 105 || lletra == 111 || lletra == 117)
                {
                    vocales = vocales + Convert.ToChar(lletra) + "";
                }
            }
            Console.WriteLine("Nuestras vocales son :" + vocales);

        }
    }
}
