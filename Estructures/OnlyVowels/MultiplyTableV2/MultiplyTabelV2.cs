﻿using System;
//Author Joan Manel
//Date 9/10/22
//Description Efectua Tabla Multiplicar
namespace MultiplyTableV2
{
    class MultiplyTabelV2
    {
        static void Main(string[] args)
        {
            int num;
            Console.WriteLine("Introduce un numero");
            num = Convert.ToInt32(Console.ReadLine());
            int tabla = 0;
            do
            {
                Console.WriteLine(tabla + "*" + num + "=" + tabla * num);
                tabla++;
            } while (tabla <= 10);
        }
    }
}
