﻿using System;
//Author Joan Manel Escudero
//Date  20/10/22
//Description Efectua Fibonnaci
namespace Fibonacci
{
    class Fibonacci
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduce un valor");
            int num = Convert.ToInt32(Console.ReadLine());
            int numero1 = 0;
            int numero2 = 1;
            int intermedio = 0;
            int fibo = 0;
            Console.WriteLine("0");
            while (numero2 < num)
            {
                Console.WriteLine(numero2);
                fibo += numero1 + numero2;
                intermedio = numero2;
                numero2 += numero1;
                numero1 = intermedio;
            }
        }
    }
}
