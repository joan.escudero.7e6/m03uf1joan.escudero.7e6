﻿using System;
//Author Joan Manel
//Date 21/10/2022
//Description Divisibles entre tres
namespace Divisibleby
{
    class Divisibleby
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Escribe un numero");
            int num = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Escribe un numero");
            int num1 = Convert.ToInt32(Console.ReadLine());
            for (int i = num; num1 > i; i++)
            {
                if (i % 3 == 0)
                    Console.WriteLine(i);
            }

        }
    }
}
