﻿using System;

namespace Exemple1
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 1;
            while (num <=100000)
            {
                Console.Write(num + " ");
                num++;
            }
        }
    }
}
