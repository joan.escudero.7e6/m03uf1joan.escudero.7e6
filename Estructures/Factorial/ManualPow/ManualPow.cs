﻿using System;
//Author Joan Manel
//Date 19/10/22
//Description potencia manual
namespace ManualPow
{
    class ManualPow
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduce un valor base");
            int base1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Introduce un valor exponente");
            int exponent = Convert.ToInt32(Console.ReadLine());
            int baseexpo = 1;
            for (int i=0; i< exponent; i++)
            {
                baseexpo = base1 * baseexpo;

            }
            Console.WriteLine("Resultado");
            Console.WriteLine(baseexpo);

        }
    }
}
