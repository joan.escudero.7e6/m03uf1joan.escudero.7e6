﻿using System;
//Author Joan Manel
//Date 20/10/22
//Description Efectua un factorial
namespace Factorial
{
    class Factorial
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ingrese un numero");
            int num = Convert.ToInt32(Console.ReadLine());
            int factorial = 1;
            for (int i = 1; i <= num; i++)
            {
                factorial = factorial * i;
            }Console.WriteLine("El factorial de " + num + " es : " + factorial);
        }
    }
}
