﻿using System;

namespace Repte_numero_secret

/*
* AUTHOR: Joan Manel Escudero Puig
* DATE: 22/10/2022
* DESCRIPTION: Intenta adivinar el numero secreto
*/

{
    public class NumeroSecreto
{
    static void Main(string[] args)
    {
        Console.WriteLine("Quieres Jugar ?");
        string respuesta = Console.ReadLine();
        if (respuesta == "si")
        {
            Console.WriteLine("He generado un numero del 1 al 100");
            Random rnd = new Random();
            int num = rnd.Next(1,101);// RANGO DE 1 A 100
            int intentos = 10;
            Boolean encertat = false;
            Console.WriteLine("Tienes 10 Intentos intenta adivinarlo");
            while (intentos > 0 && encertat == false)
            {
                Console.WriteLine("Pon un numero");
                int tirada = Convert.ToInt32(Console.ReadLine());
                intentos--;
                if (tirada == num)
                {
                    encertat = true;
                }
                else if (tirada > num)
                {
                    Console.WriteLine("El numero a buscar es menor a " + tirada);
                }
                else
                {
                    Console.WriteLine("El numero a buscar es mayor a " + tirada);

                }
            if (encertat == true)
               {
                  Console.WriteLine("Bravo!!! Has adivinado el numero en " + (10-intentos) + " intentos");
               }
            else
               {
                  Console.WriteLine("Vaya... No lo adivinaste el numero era " + num + " prueba a la proxima");
               }
            }
        }
        else
        {
            Console.WriteLine("adios");
        }

    }
}
}
