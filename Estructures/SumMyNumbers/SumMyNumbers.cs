﻿using System;
//Author Joan Manel
//Date 19/10/22
//Description suma de mis  numeros
namespace SumMyNumbers
{
    class SumMyNumbers
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Escribe un numero");
            int num = Convert.ToInt32(Console.ReadLine());
            int valor=0;
            while (num != 0) {
                valor = valor + (num%10);
                num /= 10;
            }
            Console.WriteLine("el resultado de la suma es " + valor);

        }
    }
}