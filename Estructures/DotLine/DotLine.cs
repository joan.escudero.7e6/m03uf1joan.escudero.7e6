﻿using System;
//Author Joan Manel
//Date 11/10/22
//Description Dot line(.....)
namespace DotLine
{
    class DotLine
    {
        static void Main(string[] args)
        {
            int num = 0;
            Console.WriteLine("Introduce un numero");
            num = Convert.ToInt32(Console.ReadLine());
            for (int secuencia = 1; secuencia <= num; secuencia++)
            {
                Console.Write(". ");
            }
        }
    }
}
