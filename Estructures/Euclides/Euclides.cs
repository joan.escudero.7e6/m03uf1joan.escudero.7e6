﻿using System;
//Author Joan Manel
//Date 21/10/2022
//Description Divisibles entre tres
namespace DotLine
{
    class DotLine
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Escribe 2 numeros");
            int num1 = Convert.ToInt32(Console.ReadLine());
            int num2 = Convert.ToInt32(Console.ReadLine());
            int r;
            while (num2 != 0)
            {
                r = num1 % num2;
                num1 = num2;
                num2 = r;
            }
            Console.WriteLine(num1);

        }
    }
}


       