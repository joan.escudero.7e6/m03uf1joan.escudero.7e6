﻿using System;
//Author Joan Manel
//Date 22/10/22
//Description Crea una piramide centrada
namespace PiramidCenter
{
    class PiramidCenter
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduce un numero");
            int num = Convert.ToInt32(Console.ReadLine());
            for (int i = 1; num >= i; i++)
            {
                for (int c = 1; c <= num - i; c++)
                {
                    Console.Write(" ");
                }
                for (int j = 0; j < i; j++)
                {
                    Console.Write("# ");
                }
                Console.WriteLine("");
            }

        }
    }
}