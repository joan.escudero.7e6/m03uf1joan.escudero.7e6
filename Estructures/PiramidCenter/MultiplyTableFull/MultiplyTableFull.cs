﻿using System;
//Author Joan Manel
//Date 19/10/22
//Description Tabla multiplicar entera
namespace MultiplyTableFull
{
    class MultiplyTableFull
    {
        static void Main(string[] args)
        {
            int num_multiplicar = 1;  
            int resultado;
            for (;num_multiplicar < 10 ;num_multiplicar++)
            {
                for(int num_veces = 1 ; num_veces<10; num_veces++)
                {
                    resultado = num_multiplicar * num_veces;
                    Console.Write(resultado + " ");
                }
                Console.WriteLine("");
            }
        }
    }
}
