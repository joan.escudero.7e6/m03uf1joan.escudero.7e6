﻿using System;
//Author Joan Manel
//Date 5/10/22
//Description Efectua una tabla de multiplicar
namespace MultiplyTable
{
    class MultiplyTable
    {
        static void Main(string[] args)
        {
            int num;
            Console.WriteLine("Introduce un numero");
            num = Convert.ToInt32(Console.ReadLine());
            int tabla = 0;
            while (tabla <= 10)
            {
                Console.WriteLine( tabla+ "*" + num +"="+ tabla * num);
                tabla++;
            }
        }
    }
}
