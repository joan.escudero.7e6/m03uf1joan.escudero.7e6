﻿using System;
//Author Joan Manel
//Date 20/10/22
//Description Crea una piramide
namespace Piramid
{
    class Piramid
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Pon un numero");
            int num = Convert.ToInt32(Console.ReadLine());
            for (int a=1;num>=a ;a++)
            {
                for(int b = 0; b < a; b++)
                {
                    Console.Write("#");
                }
                Console.WriteLine("");
            }
        }
    }
}
