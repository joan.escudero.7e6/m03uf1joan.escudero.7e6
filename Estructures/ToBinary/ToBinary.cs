﻿using System;
//Author Joan Manel
//Date 19/10/22
//Description Pasa un numero a binario
namespace ToBinary
{
    class ToBinary
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Escribe un numero");
            int num;
            string relleno = "";
            do
            {
                num = Convert.ToInt32(Console.ReadLine());
                if (num >= 256)
                    Console.WriteLine("error ,debe pone run numero menor a 256");

            } while (num >= 256); 
               
            while (num > 0)
                {
                    relleno = Convert.ToString(num % 2) + relleno;
                    num /= 2;

                }
            Console.Write(relleno);
           
        }
    }
}