﻿using System;
//Author Joan Manel
//Date 9/10/22
//Description Contar con do while
namespace LetsCountv2
{
    class Letscountv2
    {
        static void Main(string[] args)
        {
            int numfinal;
            numfinal = Convert.ToInt32(Console.ReadLine());
            int nums = 1;
            do
            {
                Console.Write(nums + " ");
                nums++;
            } while (nums <= numfinal);
        }
    }
}
