﻿using System;
//Author Joan Manel
//Date 5/10/22
//Description Contar hasta el num que puso el usuario
namespace LetsCount
{
    class LetsCount
    {
        static void Main(string[] args)
        {
            int numfinal;
            numfinal = Convert.ToInt32(Console.ReadLine());
            int nums = 1;
            while (nums <= numfinal)
            {
                Console.Write(nums + " ");
                nums++;
            }Console.WriteLine("\n \"Prem enter per continuar\"");
            Console.ReadLine();
        }
    }
}
