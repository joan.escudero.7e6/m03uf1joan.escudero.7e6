﻿using System;
//Author Joan Manel
//Date 6/10/22
//Description Pide numeros hasta que el usuario ponga un 5
namespace WaitFor5
{
    class WaitFor5
    {
        static void Main(string[] args)
        {
            int num;
            Console.WriteLine("Introduce un numero");
            num = Convert.ToInt32(Console.ReadLine());
            while (num != 5)
            {
                Console.WriteLine("Vuelve a introducir un numero");
                num = Convert.ToInt32(Console.ReadLine());

            }
            Console.WriteLine("Numero " + num + " encontrado");
        }  

    }
}
